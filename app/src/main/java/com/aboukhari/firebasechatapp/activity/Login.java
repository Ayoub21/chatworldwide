package com.aboukhari.firebasechatapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.aboukhari.firebasechatapp.R;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.widget.LoginButton;
import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;


public class Login extends Activity implements View.OnClickListener {

    EditText editTextName;
    Button buttonLogin;
    Button buttonRooms;
    LoginButton loginButton;
    private Firebase ref;
    CallbackManager callbackManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);
        editTextName = (EditText) findViewById(R.id.et_name);
        buttonLogin = (Button) findViewById(R.id.btn_login);
        buttonRooms = (Button) findViewById(R.id.btn_rooms);
        buttonLogin.setOnClickListener(this);
        buttonRooms.setOnClickListener(this);
        ref = new Firebase(getString(R.string.firebase_url));

        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("user_friends", "email");

        ProfileTracker profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                Log.d("natija", "profile change");
                onFacebookAccessTokenChange(AccessToken.getCurrentAccessToken());
            }
        };

        profileTracker.startTracking();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onClick(View v) {
        if (v == buttonLogin) {
          /*  if (ref.getAuth() != null) {
                ChatRoom.USERNAME = ref.getAuth().getProviderData().get("displayName").toString();
                Intent intent = new Intent(this, Friends.class);
                startActivity(intent);
            }*/

            check("boukhar@gmail.com", "SSSS");


        }
        if (v == buttonRooms) {
            loginUser("boukhar@gmail.com", "SSSS");
           /* ChatRoom.USERNAME = editTextName.getText().toString();
            Intent intent = new Intent(this, Rooms.class);
            startActivity(intent);*/
        }
    }


    private void onFacebookAccessTokenChange(final AccessToken token) {
        if (token != null) {

            ref.authWithOAuthToken("facebook", token.getToken(), new Firebase.AuthResultHandler() {
                @Override
                public void onAuthenticated(AuthData authData) {
                    Log.d("natija", "authData " + authData.getExpires());
                    Map<String, String> map2 = new HashMap<>();

                    map2.put("provider", authData.getProvider());
                    if (authData.getProviderData().containsKey("id")) {
                        map2.put("provider_id", authData.getProviderData().get("id").toString());
                    }
                    if (authData.getProviderData().containsKey("displayName")) {
                        map2.put("displayName", authData.getProviderData().get("displayName").toString());
                    }

                    if (authData.getProviderData().containsKey("profileImageURL")) {
                        map2.put("profileImageURL", authData.getProviderData().get("profileImageURL").toString());
                    }

                    if (authData.getProviderData().containsKey("email")) {
                        map2.put("email", authData.getProviderData().get("email").toString());
                    }

                    ref.child("users").child(authData.getUid()).setValue(map2);

                }

                @Override
                public void onAuthenticationError(FirebaseError firebaseError) {
                    Log.e("natija", "error " + firebaseError.getMessage());
                }
            });
        } else {
        /* Logged out of Facebook so do a logout from the Firebase app */
            ref.unauth();
        }
    }

    private void getHashKey() {
        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("com.aboukhari.firebasechatapp", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }

    private void createUser(String email, String password) {
        ref.createUser(email, password, new Firebase.ValueResultHandler<Map<String, Object>>() {
            @Override
            public void onSuccess(Map<String, Object> result) {
                Log.d("Natija", "Successfully created user account with uid: " + result.get("uid"));
            }

            @Override
            public void onError(FirebaseError firebaseError) {
                Log.e("natija", "error " + firebaseError);

            }
        });
    }

    private void check(final String email, final String password) {
        ref.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean exists = false;
                for (DataSnapshot child : snapshot.getChildren()) {

                    if (child.child("email").getValue()==email) {
                        exists = true;
                        break;
                    }
                }

                if (!exists) {
                    Log.d("natija","user already exists");
                  //  createUser(email, password);
                }
                else{
                    Log.d("natija","user already exists");
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
    }

    private void loginUser(final String email, String password) {

        ref.authWithPassword(email, password, new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                Log.d("natija", "User ID: " + authData.getUid() + ", Provider: " + authData.getProvider());
            }

            @Override
            public void onAuthenticationError(FirebaseError firebaseError) {
                Log.e("natija", "error " + firebaseError);
            }
        });
    }


}
