package com.aboukhari.firebasechatapp.firebase.manager;

/**
 * Created by aboukhari on 17/07/2015.
 */
public interface Manager {

    void loginWithFacebook();

    void loginWithEmailAndPassword(String email,String password);

    void addMessage();

    void addRoom();

    void deleteRoom();

    void addFriend();

    void deleteFriend();

    void editProfile();
}
