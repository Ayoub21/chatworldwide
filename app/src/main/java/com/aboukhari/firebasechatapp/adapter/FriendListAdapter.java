package com.aboukhari.firebasechatapp.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aboukhari.firebasechatapp.R;
import com.aboukhari.firebasechatapp.Utils.LoadImage;
import com.aboukhari.firebasechatapp.model.Friend;
import com.firebase.client.Query;

/**
 * Created by aboukhari on 16/07/2015.
 */
public class FriendListAdapter extends FirebaseListAdapter<Friend> {

    /**
     * @param ref      The Firebase location to watch for data changes. Can also be a slice of a location, using some
     *                 combination of <code>limit()</code>, <code>startAt()</code>, and <code>endAt()</code>,
     * @param layout   This is the layout used to represent a single list item. You will be responsible for populating an
     *                 instance of the corresponding view with the data from an instance of modelClass.
     * @param activity The activity containing the ListView
     */
    public FriendListAdapter(Query ref, int layout, Activity activity) {
        super(ref, Friend.class, layout, activity);
    }

    @Override
    protected void populateView(View view, Friend friend) {
        ImageView imageView = ((ImageView) view.findViewById(R.id.iv_profile));
        new LoadImage(imageView).execute(friend.getImageUrl());
        ((TextView) view.findViewById(R.id.tv_display_name)).setText(friend.getDisplayName());
    }
}
