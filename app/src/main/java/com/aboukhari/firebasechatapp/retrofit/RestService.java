package com.aboukhari.firebasechatapp.retrofit;

import com.google.gson.JsonElement;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by aboukhari on 10/04/2015.
 */
public interface RestService {


    String API_KEY = "trnsl.1.1.20150716T152343Z.fa3f8e0911e22061.fd6bf080e9fe92b230dc3b3ac215c50c9169ce94";

    @GET("/tr.json/translate")
    void translate(@Query("key") String key, @Query("lang") String lang, @Query("text") String text, Callback<JsonElement> callback);

}

